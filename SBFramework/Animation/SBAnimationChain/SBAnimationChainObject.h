//
//  SBAnimationChainObject.h
//  GraphsTest
//
//  Created by Wim Haanstra on 5/29/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnimationExtensions.h"

typedef void(^CodeBlock)(void);

@interface SBAnimationChainObject : NSObject

@property (nonatomic, copy) CodeBlock codeBlock;
@property (nonatomic, copy) CodeBlock finishedBlock;
@property (nonatomic) UIViewAnimationCurve animationCurve;

@end

