//
//  SBAnimationChain.h
//  GraphsTest
//
//  Created by Wim Haanstra on 5/28/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "SBAnimationChainObject.h"
#import "AnimationExtensions.h"

@interface SBAnimationChain : NSObject
{
	/* when you call the `stopAnimation` method, this value will be set to YES */
	BOOL					stopAnimation;
}

/* Contains the animation items encapsuled in SBAnimationChainObject types */
@property (nonatomic, retain) NSMutableArray* animationItems;

/* Contains the current index of the animation that is performing */
@property (nonatomic, readonly) NSUInteger	 currentAnimationIndex;

/* Contains the time (in float) each animation block takes */
@property (nonatomic, readonly) CGFloat currentAnimationBlockTime;

/* With this property you can set how long the total animation takes */
@property CGFloat totalAnimationTime;

/* Add an animation/code block to the animation chain, you can also specify a finishedBlock which is performed after
 this specific animation is finished. The `animationCurve` is the UIViewAnimationCurve used for this animation */
- (void) addAnimationBlock:(void(^)(void)) codeBlock finishedBlock:(void(^)(void)) finishedBlock withAnimationCurve:(UIViewAnimationCurve) animationCurve;

/* When you are done adding animations, call this to start the animation */
- (void) startAnimation;

/* Want to start an animation from a certain index, call this, the `totalAnimationTime` is shortened by the amount of animations skipped */
- (void) startAnimationFromIndex:(NSUInteger) index;

/* Calling this will stop the animationChain, but the currently running animation will continue till it is finished */
- (void) stopAnimation;

/* Calling this will start the animationChain again from where you called `stopAnimation`.*/
- (void) resumeAnimation;

/* clears the animationChain, ready for reuse */
- (void) clearAnimation;

@end
