//
//  SBAlertView.h
//  SBFramework
//
//  Created by Wim Haanstra on 6/19/12.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^buttonCodeblock)(void);

@interface SBAlertView : NSObject <UIAlertViewDelegate>
{
    NSMutableDictionary* buttonBlocks;
}

@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* message;
@property (nonatomic, retain) NSString* cancelButtonTitle;
@property (nonatomic, copy) buttonCodeblock cancelButtonCodeBlock;

@end
