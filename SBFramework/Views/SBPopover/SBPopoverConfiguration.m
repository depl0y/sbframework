//
//  SBPopoverConfiguration.m
//  WorkPlace
//
//  Created by Wim Haanstra on 1/10/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import "SBPopoverConfiguration.h"

@implementation SBPopoverConfiguration

+ (SBPopoverConfiguration*) defaultOptions {
	
	SBPopoverConfiguration* config = [[SBPopoverConfiguration alloc] init];
	return config;
	
}

- (id) init {
	self = [super init];
	
	if (self) {
		
		_darkenBackground = NO;
		_closeOnClickOutside = NO;
		
		_titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
		_titleFontColor = [UIColor whiteColor];
		_titleHeight = 20.0f;
		_titleTextAlignment = NSTextAlignmentCenter;

		_menuItemFont = [UIFont fontWithName:@"HelveticaNeue" size:14];
		_menuItemFontColor = [UIColor whiteColor];
		_menuItemHeight = 36.0f;
		_menuItemTextAlignment = UIControlContentHorizontalAlignmentLeft;
		
		_padding = 10.0f;
		_maximumWidth = 200.0f;
		_showDividerBetweenViews = YES;
	}
	
	return self;
}

@end
