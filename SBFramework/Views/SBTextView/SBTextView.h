//
//  SBTextView.h
//  SBFramework
//
//  Created by Wim Haanstra on 1/18/13.
//  Copyright (c) 2013 Sorted Bits. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBTextView : UIView <UITextViewDelegate> {
	UILabel* _placeholder;
}

// Direct access to the textview which we use
@property (nonatomic, retain) UITextView* textView;

// The maximum number of lines in the SBTextView
// Default value: 20
@property (nonatomic) NSInteger maximumLines;

// The maximum number of characters in one line
// Default value: 30
@property (nonatomic) NSInteger maximumLineLength;

// Just sends the normal UITextViewDelegate methods
@property (nonatomic, assign) id<NSObject, UITextViewDelegate> delegate;

// When the SBTextView contains no text, the placeholder will be shown
// Default value: Your text here
@property (nonatomic, retain) NSString* placeholderString;


@end
