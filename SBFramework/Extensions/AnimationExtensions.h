//
//  AnimationExtensions.h
//  SBFramework
//
//  Created by Wim Haanstra on 6/15/12.
//  Copyright (c) 2012 Sorted Bits. All rights reserved.
//
//

#import <UIKit/UIKit.h>

static inline UIViewAnimationOptions UIViewAnimationOptionWithCurve(UIViewAnimationCurve curve)
{
	switch (curve) {
		case UIViewAnimationCurveEaseInOut:
			return UIViewAnimationOptionCurveEaseInOut;
		case UIViewAnimationCurveEaseIn:
			return UIViewAnimationOptionCurveEaseIn;
		case UIViewAnimationCurveEaseOut:
			return UIViewAnimationOptionCurveEaseOut;
		case UIViewAnimationCurveLinear:
			return UIViewAnimationOptionCurveLinear;
	}
}

