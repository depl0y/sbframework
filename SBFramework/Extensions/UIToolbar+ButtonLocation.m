//
//  UIToolbar+ButtonLocation.m
//  WorkPlace
//
//  Created by Wim Haanstra on 1/9/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import "UIToolbar+ButtonLocation.h"

@implementation UIToolbar (ButtonLocation)

+ (CGRect) frameOfUIBarButtonItem:(UIBarButtonItem*) item {
	return CGRectZero;
}

- (UIButton*) buttonForUIBarButtonItem:(UIBarButtonItem*) item {
	UIButton *button = nil;
	
	for (UIView *subview in self.subviews)
	{
		if ([[subview class].description isEqualToString:@"UIToolbarButton"])
		{
			for (id target in [(UIButton *)subview allTargets])
			{
				if (target == item)
				{
					button = (UIButton *)subview;
					break;
				}
			}
			if (button != nil) break;
		}
	}
	
	return button;
	/*
	if (button != nil)
		return button.frame;
	else
		return CGRectZero;*/
}

- (CGRect) frameOfUIBarButtonItem:(UIBarButtonItem*) item {
	
	UIButton *button = [self buttonForUIBarButtonItem:item];
	
	if (button != nil)
		return button.frame;
	else
		return CGRectZero;
}

@end
