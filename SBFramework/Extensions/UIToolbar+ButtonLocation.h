//
//  UIToolbar+ButtonLocation.h
//  WorkPlace
//
//  Created by Wim Haanstra on 1/9/13.
//  Copyright (c) 2013 Wim Haanstra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIToolbar (ButtonLocation)

+ (UIButton*) buttonForUIBarButtonItem:(UIBarButtonItem*) item;
+ (CGRect) frameOfUIBarButtonItem:(UIBarButtonItem*) item;

- (UIButton*) buttonForUIBarButtonItem:(UIBarButtonItem*) item;
- (CGRect) frameOfUIBarButtonItem:(UIBarButtonItem*) item;

@end
